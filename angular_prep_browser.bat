@echo off
echo Copying cordova.js :
xcopy platforms\browser\platform_www\cordova.js src\assets\cordova /y |find "copi"
echo Copying cordova_plugins.js :
xcopy platforms\browser\platform_www\cordova_plugins.js src\assets\cordova /y |find "copi"
echo Copying plugins :
xcopy platforms\browser\platform_www\plugins src\plugins /s /e /y |find "copi"