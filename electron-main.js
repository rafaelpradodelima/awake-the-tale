const electron = require('electron');
const path = require('path');
const url = require('url');
const {app, BrowserWindow} = electron;

let mainWindow;

function createWindow() {
	const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize;

	let windowOptions = {
		width: width,
		height: height,
		frame: false,
		show: false
	};

	mainWindow = new BrowserWindow(windowOptions);

	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'www', 'index.html'),
		protocol: 'file:',
		slashes: true
	}));

	mainWindow.once('ready-to-show', () => {
		mainWindow.show()
	});

	//mainWindow.webContents.openDevTools();

	mainWindow.on('closed', function () {
		mainWindow = null
	});
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', function () {
	if (mainWindow === null) {
		createWindow();
	}
});