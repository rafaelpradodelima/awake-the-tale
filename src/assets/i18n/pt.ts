import {Language} from "../../app/types/language/language";

export const pt: Language = {
	name: "Português",
	abbreviation: "pt",
	navigationMessages: {
		start: "Início",
		navigation: "Navegação",
		buyYours: "Compre o Seu!",
		howToUse: "Como usar?",
		packages: "Pacotes Escolares"
	},
	footerMessages: {
		companyFullName: "Desperte o Conto em Você",
		moreLinks: "Mais Links"
	},
	breadcrumbMessages: {
		landing: "Página Inicial"
	},
	toastMessages: {
		closeAll: "Limpar Tudo"
	},
	landingMessages: {
		welcomeMessage: "Bem vindo!",
		introductionButtonMessage: "Descubra a magia dos contos!",
		introductionMessage: "Com este livro você conhecerá lindas histórias e viverá experiências incríveis com o aplicativo \"Desperte o Conto em Você\"!",
		singlePurchaseMessage: "Adquira já seu exemplar!",
		tenItemsPackageMessage: "Compre mais livros para a biblioteca da sua escola com 10% de desconto e frete grátis!",
		twentyItemsPackageMessage: "Compre mais livros para a biblioteca da sua escola com 10% de desconto, frete grátis e ainda 20 revistas com atividades de brinde!",
		howToUseButtonMessage: "Entenda o Aplicativo",
		buyButtonMessage: "Comprar",
		buyYoursMessage: "Compre o seu!",
		howToUseMessage: "Como Usar?",
		howToUseStepOneMessage: "Leia o livro e reflita sobre as histórias!",
		howToUseStepTwoMessage: "Instale o Aplicativo e siga suas instruções para ler os QR Codes do livro e responder aos quizzes!",
		howToUseStepThreeMessage: "Colecione medalhas e troféus, faça os desafios e tente bater seu record!",
		downloadAppMessage: "Baixar",
		packagesMessage: "Pacotes Escolares",
		singlePurchasePrice: "R$39,90",
		tenItemsPackagePrice: "10% Off",
		twentyItemsPackagePrice: "10% Off",
		singlePurchaseUrl: "https://produto.mercadolivre.com.br/MLB-1083547011-livro-desperte-o-conto-em-voc-_JM",
		downloadAppUrl: "https://play.google.com/store/apps/details?id=br.com.idsoftwares.AppDesperteOConto",
		tenItemsPackageUrl: "https://produto.mercadolivre.com.br/MLB-1078790130-kit-de-10-livros-desperte-o-conto-em-voc-_JM",
		twentyItemsPackageUrl: "https://produto.mercadolivre.com.br/MLB-1078786656-kit-desperte-o-conto-20-livros-20-revistas-de-atividades-_JM"
	}
};