import {Language} from "../../app/types/language/language";

export const en: Language = {
	name: "English",
	abbreviation: "en",
	navigationMessages: {
		start: "Start",
		navigation: "Navigation",
		buyYours: "Buy Yours!",
		howToUse: "How to Use",
		packages: "Packages for Schools"
	},
	footerMessages: {
		companyFullName: "Awaken the Tale in You",
		moreLinks: "More Links"
	},
	breadcrumbMessages: {
		landing: "Home"
	},
	toastMessages: {
		closeAll: "Clear All"
	},
	landingMessages: {
		welcomeMessage: "Welcome!",
		introductionButtonMessage: "Discover the tales' magic!",
		introductionMessage: "With this book you will meet beautiful stories and live incredible experiences with the app \"Awaken the Tale in You\"!",
		singlePurchaseMessage: "Get yours now!",
		tenItemsPackageMessage: "Buy more books for your school's library with 10% off and free shipping!",
		twentyItemsPackageMessage: "Buy more books for your school's library with 10% off, free shipping and 20 activity magazines as a gift!",
		howToUseButtonMessage: "How to Use the App",
		buyButtonMessage: "Buy",
		buyYoursMessage: "Buy Yours!",
		howToUseMessage: "How to Use",
		packagesMessage: "Packages for Schools",
		howToUseStepOneMessage: "Read the book and think about the stories!",
		howToUseStepTwoMessage: "Install the app and follow the instructions to read the QR Codes from the book and answer the quizzes!",
		howToUseStepThreeMessage: "Collect medals and trophies, complete the challenges and try to beat your record!",
		downloadAppMessage: "Download",
		singlePurchasePrice: "R$39,90",
		tenItemsPackagePrice: "10% Off",
		twentyItemsPackagePrice: "10% Off",
		singlePurchaseUrl: "https://produto.mercadolivre.com.br/MLB-1083547011-livro-desperte-o-conto-em-voc-_JM",
		downloadAppUrl: "https://play.google.com/store/apps/details?id=br.com.idsoftwares.AppDesperteOConto",
		tenItemsPackageUrl: "https://produto.mercadolivre.com.br/MLB-1078790130-kit-de-10-livros-desperte-o-conto-em-voc-_JM",
		twentyItemsPackageUrl: "https://produto.mercadolivre.com.br/MLB-1078786656-kit-desperte-o-conto-20-livros-20-revistas-de-atividades-_JM"
	}
};