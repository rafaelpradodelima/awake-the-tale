import {Language} from "../../app/types/language/language";

export const es: Language = {
	name: "Español",
	abbreviation: "es",
	navigationMessages: {
		start: "Página de inicio",
		navigation: "Navegación",
		buyYours: "Compra el tuyo",
		howToUse: "Cómo utilizar",
		packages: "Paquetes para Escuelas"
	},
	footerMessages: {
		companyFullName: "Despierta el Cuento en Ti",
		moreLinks: "Más enlaces"
	},
	breadcrumbMessages: {
		landing: "Página de Inicio"
	},
	toastMessages: {
		closeAll: "Limpiar todo"
	},
	landingMessages: {
		welcomeMessage: "¡Bienvenido!",
		introductionButtonMessage: "¡Descubre la magia de los cuentos!",
		introductionMessage: "Con este libro conocerás hermosas historias y vivirás experiencias increíbles con la aplicación. ¡\"Despierta el Cuento en Ti\"!",
		singlePurchaseMessage: "¡Consigue el tuyo ahora!",
		tenItemsPackageMessage: "¡Compra más libros para la biblioteca de tu escuela con un 10% de descuento y envío gratis!",
		twentyItemsPackageMessage: "¡Compre más libros para la biblioteca de su escuela con un 10% de descuento, envío gratis y 20 revistas de actividades como regalo!",
		howToUseButtonMessage: "Cómo usar la aplicación",
		buyButtonMessage: "Comprar",
		buyYoursMessage: "¡Compra el tuyo!",
		howToUseMessage: "Cómo utilizar",
		packagesMessage: "Paquetes para Escuelas",
		howToUseStepOneMessage: "¡Lee el libro y piensa en las historias!",
		howToUseStepTwoMessage: "¡Instala la aplicación y sigue las instrucciones para leer los códigos QR del libro y contestar las preguntas!",
		howToUseStepThreeMessage: "¡Consigue medallas y trofeos, completa los desafíos e intenta batir tu récord!",
		downloadAppMessage: "Descargar",
		singlePurchasePrice: "R$39,90",
		tenItemsPackagePrice: "10% Off",
		twentyItemsPackagePrice: "10% Off",
		singlePurchaseUrl: "https://produto.mercadolivre.com.br/MLB-1083547011-livro-desperte-o-conto-em-voc-_JM",
		downloadAppUrl: "https://play.google.com/store/apps/details?id=br.com.idsoftwares.AppDesperteOConto",
		tenItemsPackageUrl: "https://produto.mercadolivre.com.br/MLB-1078790130-kit-de-10-livros-desperte-o-conto-em-voc-_JM",
		twentyItemsPackageUrl: "https://produto.mercadolivre.com.br/MLB-1078786656-kit-desperte-o-conto-20-livros-20-revistas-de-atividades-_JM"
	}
};