import {NgModule} from '@angular/core';
import {FlexLayoutModule} from "@angular/flex-layout";
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {NgxElectronModule} from 'ngx-electron';
import {PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';

import {SplashScreenService} from './services/ui/splash-screen.service';
import {LanguageService} from './services/ui/language.service';
import {ToastService} from "./services/ui/toast.service";
import {TranslationLoaderService} from "./services/ui/translation-loader.service";
import {RootScrollService} from "./services/ui/root-scroll.service";

import {AppRoutingModule} from './app-routing.module';
import {RootComponent} from './components/core/root/root.component';
import {ToolbarComponent} from './components/core/toolbar/toolbar.component';
import {NavigationComponent} from './components/core/navigation/navigation.component';
import {BreadcrumbsComponent} from './components/core/breadcrumbs/breadcrumbs.component';
import {FooterComponent} from './components/core/footer/footer.component';

import {BackgroundImageDirective} from './directives/background-image.directive';
import {BorderImageDirective} from './directives/border-image.directive';

import {LandingComponent} from './components/pages/landing/landing.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
	suppressScrollX: true
};

@NgModule({
	declarations: [
		RootComponent,
		ToolbarComponent,
		NavigationComponent,
		BreadcrumbsComponent,
		FooterComponent,
		LandingComponent,
		BackgroundImageDirective,
		BorderImageDirective
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FlexLayoutModule,
		NgxElectronModule,
		PerfectScrollbarModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useClass: TranslationLoaderService
			}
		}),
		AppRoutingModule
	],
	providers: [
		SplashScreenService,
		LanguageService,
		ToastService,
		RootScrollService,
		{
			provide: PERFECT_SCROLLBAR_CONFIG,
			useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
		}
	],
	bootstrap: [RootComponent]
})
export class AppModule {
	constructor() {
	}
}