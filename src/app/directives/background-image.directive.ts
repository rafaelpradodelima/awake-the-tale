import {AfterViewInit, Directive, ElementRef, Input} from '@angular/core';

@Directive({
	selector: '[appBackgroundImage]'
})
export class BackgroundImageDirective implements AfterViewInit {
	@Input() imageSource: string;

	constructor(private elementRef: ElementRef) {
	}

	ngAfterViewInit() {
		this.elementRef.nativeElement.style.backgroundImageSource = 'url(' + this.imageSource + ') no-repeat center center';
		this.elementRef.nativeElement.style.backgroundSize = 'contain';
	}
}
