import {AfterViewInit, Directive, ElementRef, Input} from '@angular/core';


@Directive({
	selector: '[appBorderImage]'
})
export class BorderImageDirective implements AfterViewInit {
	@Input() imageSource: string;

	constructor(private elementRef: ElementRef) {
	}

	ngAfterViewInit() {
		this.elementRef.nativeElement.style.border = '10px solid transparent';
		this.elementRef.nativeElement.style.borderImage = 'url(' + this.imageSource + ') 30 round';
	}
}
