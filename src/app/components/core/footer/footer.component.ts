import {AfterViewInit, Component} from '@angular/core';
import {Language} from '../../../types/language/language';
import {LanguageService} from '../../../services/ui/language.service';
import {FooterMessages} from '../../../types/language/core/footer-messages';

declare var $: any;

@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.css']
})
export class FooterComponent implements AfterViewInit {
	footerMessages: FooterMessages;
	currentYear: number;

	constructor(private languageService: LanguageService) {
		this.footerMessages = new FooterMessages();
		this.currentYear = new Date().getFullYear();

		this.subscribeLanguage();
	}

	ngAfterViewInit() {
		this.removePaddingFromFooterIfNoContent();
	}

	private removePaddingFromFooterIfNoContent() {
		let pageFooter = $('.page-footer');

		if (pageFooter.children().length == 1) {
			// noinspection TypeScriptValidateJSTypes
			pageFooter.css('padding-top', '0');
		}
	}

	private subscribeLanguage() {
		this.languageService.currentLanguage.subscribe((response: Language) => {
			this.footerMessages = response.footerMessages;
		});
	}
}
