import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {SplashScreenService} from '../../../services/ui/splash-screen.service';
import {ElectronService} from "ngx-electron";
import {RootScrollService} from "../../../services/ui/root-scroll.service";
import {BehaviorSubject} from "rxjs";
import {PerfectScrollbarComponent} from 'ngx-perfect-scrollbar';

declare var $: any;

@Component({
	selector: 'app-root',
	templateUrl: './root.component.html',
	styleUrls: ['./root.component.css']
})
export class RootComponent implements AfterViewInit {
	private scrolledDownSource: BehaviorSubject<any>;
	private scrolledToTopSource: BehaviorSubject<any>;
	private directiveSource: BehaviorSubject<any>;

	@ViewChild(PerfectScrollbarComponent) scrollbarComponent: PerfectScrollbarComponent;

	constructor(private splashScreenService: SplashScreenService, private rootScrollService: RootScrollService, public electronService: ElectronService) {
		this.scrolledDownSource = new BehaviorSubject<any>(undefined);
		this.scrolledToTopSource = new BehaviorSubject<any>(undefined);
		this.directiveSource = new BehaviorSubject<any>(undefined);

		this.rootScrollService.scrolledDown = this.scrolledDownSource.asObservable();
		this.rootScrollService.scrolledToTop = this.scrolledToTopSource.asObservable();
		this.rootScrollService.scrollBarDirectiveObservable = this.directiveSource.asObservable();

		this.rootScrollService.subscribeScrollBarDirective();
	}

	ngAfterViewInit() {
		this.initializeToolbarIfElectronApp();
		this.transmitScrollBarDirective();
	}

	private transmitScrollBarDirective() {
		this.directiveSource.next(this.scrollbarComponent.directiveRef);
	}

	private initializeToolbarIfElectronApp() {
		if (this.electronService.isElectronApp) {
			$('nav').css('margin-top', '+=32');

			if (screen.width > 599) {
				$('main').css('height', 'calc(100vh - 160px)');
			} else {
				$('main').css('height', 'calc(100vh - 144px)');
			}
		}
	}

	closeDropdownOnScroll() {
		$('.dropdown-trigger').dropdown('close');
	}

	transmitScrolledDown(event) {
		this.scrolledDownSource.next(event);
	}

	transmitScrolledToTop(event) {
		this.scrolledToTopSource.next(event);
	}
}