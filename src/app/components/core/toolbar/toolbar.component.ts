import {Component} from '@angular/core';
import {ElectronService} from "ngx-electron";

@Component({
	selector: 'app-toolbar',
	templateUrl: './toolbar.component.html',
	styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {
	constructor(private electronService: ElectronService) {
	}

	closeWindow() {
		window.close();
	}

	minimizeWindow() {
		this.electronService.remote.BrowserWindow.getFocusedWindow().minimize();
	}

	maximizeOrRestoreWindow() {
		if (this.electronService.remote.BrowserWindow.getFocusedWindow().isMaximized()) {
			this.electronService.remote.BrowserWindow.getFocusedWindow().unmaximize();
		} else {
			this.electronService.remote.BrowserWindow.getFocusedWindow().maximize();
		}
	}
}
