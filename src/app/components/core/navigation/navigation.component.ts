import {AfterViewInit, Component} from '@angular/core';
import {LanguageService} from '../../../services/ui/language.service';
import {Language} from '../../../types/language/language';
import {NavigationMessages} from '../../../types/language/core/navigation-messages';
import {RootScrollService} from "../../../services/ui/root-scroll.service";

declare var $: any;

@Component({
	selector: 'app-navigation',
	templateUrl: './navigation.component.html',
	styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements AfterViewInit {
	currentLanguage: Language;
	availableLanguages: Language[];
	navigationMessages: NavigationMessages;

	constructor(private languageService: LanguageService, private rootScrollService: RootScrollService) {
		this.currentLanguage = new Language();
		this.navigationMessages = this.currentLanguage.navigationMessages;
		this.availableLanguages = [];

		this.subscribeLanguage();
		this.subscribeLanguagesAvailable();
	}

	ngAfterViewInit() {
		this.initializeDropdown();
		this.initializeSideNav();
	}

	private subscribeLanguagesAvailable() {
		this.languageService.allLanguages.subscribe((response: Language[]) => {
			this.availableLanguages = response;
		});
	}

	private subscribeLanguage() {
		this.languageService.currentLanguage.subscribe((response: Language) => {
			this.currentLanguage = response;
			this.navigationMessages = response.navigationMessages;
		});
	}

	private initializeDropdown() {
		$('.dropdown-trigger').dropdown({
			coverTrigger: false,
			alignment: "right",
			constrainWidth: false,
			container: $(".dropdowns-container")
		});
	}

	private initializeSideNav() {
		$('.sidenav').sidenav();
	}

	changeLanguage(newLanguage: string) {
		this.languageService.changeLanguage(newLanguage);
	}

	goToElement(element) {
		this.rootScrollService.scrollToElement(element);
		$('.sidenav').sidenav('close');
	}
}
