import {Component} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Breadcrumb} from '../../../types/core/breadcrumb';
import {Language} from '../../../types/language/language';
import {LanguageService} from '../../../services/ui/language.service';
import {BreadCrumbMessages} from '../../../types/language/core/breadcrumb-messages';

@Component({
	selector: 'app-breadcrumbs',
	templateUrl: './breadcrumbs.component.html',
	styleUrls: ['./breadcrumbs.component.css']
})
export class BreadcrumbsComponent {
	breadcrumbs: Breadcrumb[];
	private breadcrumbMessages: BreadCrumbMessages;

	constructor(private router: Router, private activatedRoute: ActivatedRoute, private languageService: LanguageService) {
		this.breadcrumbMessages = new BreadCrumbMessages();
		this.breadcrumbs = [];

		this.subscribeRoute();
		this.subscribeLanguage();
	}

	private subscribeRoute() {
		this.router.events.subscribe(event => {
			if (event instanceof NavigationEnd) {
				this.breadcrumbs = this.generateBreadcrumbs(this.activatedRoute.root);
			}
		});
	}

	private subscribeLanguage() {
		this.languageService.currentLanguage.subscribe((response: Language) => {
			this.breadcrumbMessages = response.breadcrumbMessages;
			this.breadcrumbs = this.generateBreadcrumbs(this.activatedRoute.root);
		});
	}

	private generateBreadcrumbs(activatedRoute: ActivatedRoute, url: string = '/', breadcrumbsFound: Breadcrumb[] = []): Breadcrumb[] {
		let currentBreadCrumbLabel: string;
		let currentBreadcrumb: Breadcrumb;
		let routeData = activatedRoute.routeConfig;

		if (routeData) {
			if (this.breadcrumbMessages[routeData.data['label']]) {
				currentBreadCrumbLabel = this.breadcrumbMessages[routeData.data['label']];
			} else {
				currentBreadCrumbLabel = routeData.data['label'];
			}

			currentBreadcrumb = new Breadcrumb(
				url + routeData.path + '/',
				currentBreadCrumbLabel
			);

			breadcrumbsFound.push(currentBreadcrumb);

			if (activatedRoute.firstChild) {
				breadcrumbsFound = this.generateBreadcrumbs(
					activatedRoute.firstChild,
					currentBreadcrumb.path,
					breadcrumbsFound
				);
			}
		} else if (activatedRoute.firstChild) {
			breadcrumbsFound = this.generateBreadcrumbs(
				activatedRoute.firstChild
			);
		}

		return breadcrumbsFound;
	}
}