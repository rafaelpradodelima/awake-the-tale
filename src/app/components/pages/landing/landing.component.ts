import {AfterViewInit, Component, OnDestroy} from '@angular/core';
import {LanguageService} from '../../../services/ui/language.service';
import {Language} from '../../../types/language/language';
import {LandingMessages} from '../../../types/language/pages/landing-messages';
import {RootScrollService} from "../../../services/ui/root-scroll.service";

declare var $: any;

@Component({
	selector: 'app-landing',
	templateUrl: './landing.component.html',
	styleUrls: ['./landing.component.css'],
})
export class LandingComponent implements AfterViewInit, OnDestroy {
	landingMessages: LandingMessages;

	constructor(private languageService: LanguageService, private rootScrollService: RootScrollService) {
		this.subscribeLanguage();
	}

	ngAfterViewInit() {
		this.initializeLandingNavBar();
		this.makeNavBarVisibleOnScrollDown();
		this.makeNavBarTransparentOnScrollToTop();
	}

	ngOnDestroy() {
		// noinspection TypeScriptValidateJSTypes
		$('.navbar-fixed').css('position', 'relative');
	}

	private makeNavBarVisibleOnScrollDown() {
		this.rootScrollService.scrolledDown.subscribe((response) => {
			if (response) {
				if (response.type == 'ps-scroll-down') {
					$('.nav-wrapper, nav').removeClass('black-shadow-background').addClass('bottom-shadow dark-primary-theme');
				}
			}
		});
	}

	private makeNavBarTransparentOnScrollToTop() {
		this.rootScrollService.scrolledToTop.subscribe((response) => {
			if (response) {
				if (response.type == 'ps-y-reach-start') {
					$('.nav-wrapper, nav').removeClass('bottom-shadow dark-primary-theme').addClass('black-shadow-background');
				}
			}
		});
	}

	private initializeLandingNavBar() {
		let navBar = $('.nav-wrapper, nav');
		// noinspection TypeScriptValidateJSTypes
		$('.navbar-fixed').css('position', 'fixed');
		// noinspection TypeScriptValidateJSTypes
		navBar.css('box-shadow', 'none');
		navBar.addClass('black-shadow-background');
	}

	private subscribeLanguage() {
		this.languageService.currentLanguage.subscribe((response: Language) => {
			this.landingMessages = response.landingMessages;
		});
	}

	goToElement(element) {
		this.rootScrollService.scrollToElement(element);
	}
}