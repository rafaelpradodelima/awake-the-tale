import {en} from "../../assets/i18n/en";
import {pt} from "../../assets/i18n/pt";
import {es} from "../../assets/i18n/es";
import {Language} from "../types/language/language";

export const languagesAvailable: Language[] = [
	pt, en, es
];