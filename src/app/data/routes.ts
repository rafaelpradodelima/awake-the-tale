import {Routes} from "@angular/router";
import {LandingComponent} from "../components/pages/landing/landing.component";

export const routes: Routes = [
	{
		path: '',
		component: LandingComponent,
		data: {
			label: 'landing'
		}
	}
];