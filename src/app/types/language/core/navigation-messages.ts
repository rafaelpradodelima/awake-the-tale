export class NavigationMessages {
	start: string;
	navigation: string;
	buyYours: string;
	howToUse: string;
	packages: string;

	constructor() {
		this.start = '';
		this.navigation = '';
		this.buyYours = '';
		this.howToUse = '';
		this.packages = '';
	}
}