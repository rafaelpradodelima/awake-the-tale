export class FooterMessages {
	companyFullName: string;
	moreLinks: string;

	constructor() {
		this.companyFullName = '';
		this.moreLinks = '';
	}
}