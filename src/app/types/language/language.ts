import {NavigationMessages} from "./core/navigation-messages";
import {FooterMessages} from "./core/footer-messages";
import {BreadCrumbMessages} from "./core/breadcrumb-messages";
import {LandingMessages} from "./pages/landing-messages";
import {ToastMessages} from "./core/toast-messages";

export class Language {
	name: string;
	abbreviation: string;

	navigationMessages: NavigationMessages;
	breadcrumbMessages: BreadCrumbMessages;
	footerMessages: FooterMessages;
	toastMessages: ToastMessages;
	landingMessages: LandingMessages;

	constructor() {
		this.name = "";
		this.abbreviation = "";
		this.navigationMessages = new NavigationMessages();
		this.breadcrumbMessages = new BreadCrumbMessages();
		this.footerMessages = new FooterMessages();
		this.toastMessages = new ToastMessages();
		this.landingMessages = new LandingMessages();
	}
}