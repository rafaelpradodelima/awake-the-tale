export class LandingMessages {
	welcomeMessage: string;
	introductionButtonMessage: string;
	introductionMessage: string;
	singlePurchaseMessage: string;
	howToUseButtonMessage: string;
	buyButtonMessage: string;
	singlePurchasePrice: string;
	singlePurchaseUrl: string;
	buyYoursMessage: string;
	howToUseMessage: string;
	howToUseStepOneMessage: string;
	howToUseStepTwoMessage: string;
	howToUseStepThreeMessage: string;
	downloadAppMessage: string;
	downloadAppUrl: string;
	packagesMessage: string;
	tenItemsPackagePrice: string;
	tenItemsPackageMessage: string;
	tenItemsPackageUrl: string;
	twentyItemsPackagePrice: string;
	twentyItemsPackageMessage: string;
	twentyItemsPackageUrl: string;

	constructor() {
		this.welcomeMessage = '';
		this.introductionMessage = '';
		this.introductionButtonMessage = '';
		this.singlePurchaseMessage = '';
		this.howToUseButtonMessage = '';
		this.buyButtonMessage = '';
		this.singlePurchasePrice = '';
		this.singlePurchasePrice = '';
		this.buyYoursMessage = '';
		this.howToUseMessage = '';
		this.howToUseStepOneMessage = '';
		this.howToUseStepTwoMessage = '';
		this.howToUseStepThreeMessage = '';
		this.downloadAppMessage = '';
		this.downloadAppUrl = '';
		this.packagesMessage = '';
		this.tenItemsPackagePrice = '';
		this.tenItemsPackageMessage = '';
		this.tenItemsPackageUrl = '';
		this.twentyItemsPackagePrice = '';
		this.twentyItemsPackageMessage = '';
		this.twentyItemsPackageUrl = '';
	}
}