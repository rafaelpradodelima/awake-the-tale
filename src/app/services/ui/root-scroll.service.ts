import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {PerfectScrollbarDirective} from 'ngx-perfect-scrollbar';
import {isUndefined} from "util";

declare var $: any;

@Injectable({
	providedIn: 'root'
})
export class RootScrollService {
	scrolledDown: Observable<any>;
	scrolledToTop: Observable<any>;
	scrollBarDirectiveObservable: Observable<any>;
	scrollBarDirective: PerfectScrollbarDirective;

	constructor() {
	}

	subscribeScrollBarDirective() {
		this.scrollBarDirectiveObservable.subscribe((response) => {
			this.scrollBarDirective = response;
		});
	}

	scrollToBottom() {
		if (!isUndefined(this.scrollBarDirective)) {
			this.scrollBarDirective.scrollToBottom(0, 100);
		} else {
			let retryInterval = setInterval(() => {
				if (!isUndefined(this.scrollBarDirective)) {
					this.scrollBarDirective.scrollToBottom(0, 100);
					clearInterval(retryInterval);
				}
			}, 500);
		}
	}

	scrollToElement(elementToGo) {
		let navHeight = $('nav').height();
		if (!isUndefined(this.scrollBarDirective)) {
			this.scrollBarDirective.scrollToElement(elementToGo, -(navHeight * 0.8), 1000);
		} else {
			let retryInterval = setInterval(() => {
				if (!isUndefined(this.scrollBarDirective)) {
					this.scrollBarDirective.scrollToElement(elementToGo, -(navHeight * 0.8), 1000);
					clearInterval(retryInterval);
				}
			}, 500);
		}
	}
}
