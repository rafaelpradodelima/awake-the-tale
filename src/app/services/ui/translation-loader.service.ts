import {languagesAvailable} from '../../data/languages';
import {TranslateLoader} from '@ngx-translate/core';
import {en} from '../../../assets/i18n/en';
import {Observable} from 'rxjs';
import {Language} from "../../types/language/language";

export class TranslationLoaderService implements TranslateLoader {

	public getTranslation(languageRequested: string): Observable<any> {
		let languageFound: Language = en;

		languagesAvailable.forEach((language: Language) => {
			if (language.abbreviation === languageRequested) {
				languageFound = language;
			}
		});

		return Observable.create(observer => {
			observer.next(languageFound);
			observer.complete();
		});
	}

}