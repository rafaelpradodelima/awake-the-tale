import {DOCUMENT} from '@angular/common';
import {ElementRef, Inject, Injectable} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {animate, AnimationBuilder, AnimationPlayer, style} from '@angular/animations';

declare var StatusBar: any;
declare var device: any;

@Injectable({
	providedIn: 'root',
})
export class SplashScreenService {
	splashScreenElement: ElementRef;
	player: AnimationPlayer;

	constructor(@Inject(DOCUMENT) private document: any, private animationBuilder: AnimationBuilder, private router: Router) {
		this.splashScreenElement = this.document.body.querySelector('#splash-screen');

		this.hideSplashScreenOnLoad();
	}

	private hideSplashScreenOnLoad() {
		if (this.splashScreenElement) {
			const hideOnLoad = this.router.events.subscribe((event) => {
					if (event instanceof NavigationEnd) {
						setTimeout(() => {
							this.hideSplashScreen();
							hideOnLoad.unsubscribe();
						}, 500);
					}
				}
			);
		}
	}

	showSplashScreen() {
		this.player = this.animationBuilder.build([
			style({
				opacity: '0',
				zIndex: '10001'
			}),
			animate('600ms ease', style({opacity: '1'}))
		]).create(this.splashScreenElement);

		setTimeout(() => {
			this.player.play();
			if (device.platform === "Android") {
				StatusBar.backgroundColorByHexString("#D7D7D7");
			}
		}, 0);
	}

	hideSplashScreen() {
		this.player = this.animationBuilder.build([
			style({opacity: '1'}),
			animate('600ms ease', style({
				opacity: '0',
				zIndex: '-10'
			}))
		]).create(this.splashScreenElement);

		setTimeout(() => {
			this.player.play();
			if (device.platform === "Android") {
				StatusBar.backgroundColorByHexString("#101D7D");
			}
		}, 0);
	}
}