import {Injectable} from '@angular/core';
import {LanguageService} from "./language.service";
import {ToastMessages} from "../../types/language/core/toast-messages";
import {Language} from "../../types/language/language";

declare var M: any;
declare var $: any;

@Injectable({
	providedIn: 'root'
})
export class ToastService {
	toastMessages: ToastMessages;

	constructor(private languageService: LanguageService) {
		this.subscribeLanguage();
		this.initializeCloseAllButton();
		this.initializeCloseSingleToastButton();
	}

	private subscribeLanguage() {
		this.languageService.currentLanguage.subscribe((response: Language) => {
			this.toastMessages = response.toastMessages;
			$("#close-all-button").html(this.toastMessages.closeAll);
		});
	}

	private initializeCloseAllButton() {
		$(document).on('click', '#close-all-button', function () {
			$('.toast, .close-all-container').fadeOut(function () {
				M.Toast.dismissAll();
			});
		});
	}

	private initializeCloseSingleToastButton() {
		$(document).on('click', '#toast-container .toast button', function () {
			$(this).parent().fadeOut(function () {
				$(this).remove();
			});

			if ($('#toast-container').children(".toast").length == 1) {
				$('.close-all-container').fadeOut(function () {
					$(this).remove();
				});
			}
		});
	}

	information(message: String) {
		M.toast({
			html: `${message}
					<button class="btn-flat toast-action white-text">
						<i class="mdi mdi-close"></i>
					</button>`,
			classes: 'info-theme'
		});
		this.addCloseAllButton();
	}

	warning(message: String) {
		M.toast({
			html: `<i class="left mdi mdi-information"></i>
					${message}
					<button class="btn-flat toast-action black-text">
						<i class="mdi mdi-close"></i>
					</button>`,
			classes: 'accent-theme',
			displayLength: 20000
		});
		this.addCloseAllButton();
	}

	danger(message: String) {
		M.toast({
			html: `<i class="left mdi mdi-alert"></i>
					${message}
					<button class="btn-flat toast-action white-text">
						<i class="mdi mdi-close"></i>
					</button>`,
			classes: 'danger-theme',
			displayLength: 7200000
		});
		this.addCloseAllButton();
	}

	addCloseAllButton() {
		let container = $('#toast-container');
		let closeAllButtonTop = `<div class='close-all-container hide-on-med-and-down'>
									<button class='btn-flat waves-effect waves-light info-theme right' id='close-all-button'>
										${this.toastMessages.closeAll}
									</button>
								</div>`;
		let closeAllButtonBottom = `<div class='close-all-container hide-on-large-only'>
										<button class='btn-flat waves-effect waves-light info-theme right' id='close-all-button'>
											${this.toastMessages.closeAll}
										</button>
									</div>`;

		if (container.children(".close-all-container").length == 0) {
			container.prepend(closeAllButtonTop).append(closeAllButtonBottom);
		} else {
			container.children(".close-all-container").remove();
			container.prepend(closeAllButtonTop).append(closeAllButtonBottom);
		}
	}
}
