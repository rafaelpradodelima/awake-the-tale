import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {pairwise} from 'rxjs/operators';
import {NavigationEnd, NavigationStart, Router, RouterEvent} from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class LastUrlService {
	lastUrl: Observable<string>;

	private lastUrlSource: BehaviorSubject<string>;

	constructor(private router: Router) {
		this.lastUrlSource = new BehaviorSubject<string>('/');
		this.lastUrl = this.lastUrlSource.asObservable();

		this.subscribeRouteAndPublishLastUrl();
	}

	private subscribeRouteAndPublishLastUrl() {
		this.router.events.pipe(pairwise()).subscribe((events: [RouterEvent, RouterEvent]) => {
			if (events[0] instanceof NavigationEnd && events[1] instanceof NavigationStart) {
				this.lastUrlSource.next(events[0].url);
			}
		});
	}
}
