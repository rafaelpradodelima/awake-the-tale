import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Language} from '../../types/language/language';
import {BehaviorSubject, Observable} from 'rxjs';
import {languagesAvailable} from "../../data/languages";

@Injectable({
	providedIn: 'root',
})
export class LanguageService {
	allLanguages: Observable<Language[]>;
	currentLanguage: Observable<Language>;
	allLanguagesAbbreviation: string[];
	currentLanguageAbbreviation: string;

	private allLanguagesSource: BehaviorSubject<Language[]>;
	private allLanguagesHelper: Language[];
	private currentLanguageSource: BehaviorSubject<Language>;

	constructor(private translateService: TranslateService) {
		let currentLanguageHelper = new Language();
		currentLanguageHelper.abbreviation = 'en';

		this.currentLanguageSource = new BehaviorSubject<Language>(currentLanguageHelper);
		this.allLanguagesSource = new BehaviorSubject<Language[]>([]);
		this.allLanguagesHelper = [];
		this.allLanguagesAbbreviation = [];
		this.allLanguages = this.allLanguagesSource.asObservable();
		this.currentLanguage = this.currentLanguageSource.asObservable();

		this.loadLanguagesAvailableAbbreviations();
		this.setDefaultLanguage();
		this.publishCurrentLanguage();
		this.publishAllLanguages();
	}

	private loadLanguagesAvailableAbbreviations() {
		languagesAvailable.forEach((language: Language) => {
			this.allLanguagesAbbreviation.push(language.abbreviation);
		});
	}

	private setDefaultLanguage() {
		if (this.allLanguagesAbbreviation.includes(this.translateService.getBrowserLang())) {
			this.translateService.setDefaultLang(this.translateService.getBrowserLang());
		} else {
			this.translateService.setDefaultLang('en');
		}

		this.currentLanguageAbbreviation = this.translateService.defaultLang;
	}

	private publishCurrentLanguage() {
		this.translateService.use(this.translateService.defaultLang).subscribe((response: Language) => {
			if (response) {
				this.currentLanguageSource.next(response);
			}
		});
	}

	private publishAllLanguages() {
		let languagesIncludedHelper: string[] = [];

		this.allLanguagesAbbreviation.forEach(language => {
			this.translateService.use(language).subscribe((response: Language) => {
				if (response) {
					if (!languagesIncludedHelper.includes(response.abbreviation)) {
						languagesIncludedHelper.push(response.abbreviation);
						this.allLanguagesHelper.push(response);
					}
				}

				this.allLanguagesSource.next(this.allLanguagesHelper);
			});
		});
	}

	changeLanguage(newLanguage: string) {
		this.currentLanguageAbbreviation = newLanguage;

		this.translateService.use(newLanguage).subscribe((response: Language) => {
			this.currentLanguageSource.next(response);
		});
	}
}